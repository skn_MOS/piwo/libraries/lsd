#Projekt PIWO
#LSD
#Light Show Debug
#Made by Jakub Olszewski 2020
from imutils.perspective import four_point_transform
from imutils import contours
from skimage import measure
import numpy as np
import imutils
import cv2
import math
import copy
import lightDetection as ld
import lightAssign as la

def dataFailureChecker():
	canPass = True
	if(podglad[0] <= 0):
		print("[Err] Niepoprawna wartość rozmiaru okna (x<1)")
		canPass = False
	elif(podglad[0] > 0 and podglad[0] < 100):
		print("[Warn] Okno podglądu może być źle widoczne (x<100)")
	elif(podglad[0] > 1920):
		print("[Warn] Okno może się nie mieścić na ekranie (x>1920)")

	if(podglad[1] <= 0):
		print("[Err] Niepoprawna wartość rozmiaru okna (y<1)")
		canPass = False
	elif(podglad[1] > 0 and podglad[1] < 100):
		print("[Warn] Okno podglądu może być źle widoczne (y<100)")
	elif(podglad[1] > 1080):
		print("[Warn] Okno może się nie mieścić na ekranie (y>1080)")

	if(oknaX <= 0):
		print("[Err] Niepoprawna ilość kolumn okien (x<1)")
		canPass = False
	if(oknaY <= 0):
		print("[Err] Niepoprawna ilość rzędów okien (y<1)")
		canPass = False

	if(calibrationAccuracy <= 0):
		print("[Err] Niepoprawna ilość klatek do kalibracji [n<1]")
		canPass = False

	if(calibrationAccuracy > 10):
		print("[Warn] Możliwy overload, zbyt dużo klatek do kalibracji [n>10]")

	if(testBool):
		cap = cv2.VideoCapture(testPath)
		ret, frame = cap.read()
		if(not ret):
			print("[Err] Nie można odczytać pliku wideo")
			canPass = False
		else:
			ih, iw, ic = frame.shape
			if(ih < 360):
				print("[Warn] Wideo ma mały rozmiar, możliwe błędy (y<360)")
			if(iw < 480):
				print("[Warn] Wideo ma mały rozmiar, możliwe błędy (x<480)")
			if(ic < 3):
				print("[Err] Wideo ma tylko jeden kanał kolorów, brak możliwości analizy RGB")
				canPass = False
		cap.release()

	return(canPass)

def calibrationFramesExtraction():

	if(testBool):
		cap = cv2.VideoCapture(testPath)#Wideo
	else:
		cap = cv2.VideoCapture(0)#Webcam feed

	ret, frame = cap.read()
	frame0 = copy.copy(frame)
	frame1 = copy.copy(frame)
	framebasic = copy.copy(frame)
	if(not ret):
		print("[Err] Brak wykrytego sygnału z kamery (wideo), przerywam")
		return

	ih, iw, ic = frame.shape
	if(not testBool):
		print("[Inf] Ustaw kamerę. Kiedy będzie w dobrej pozycji, kliknij 'q'")

		while(True):
			ret, frame = cap.read()
			cv2.imshow("Ustawianie kamery", cv2.resize(frame,(podglad[0], podglad[1])))
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
	cv2.destroyAllWindows()


	ret, frame = cap.read()
	frames = [[],[],[]] #RGB
	isLight, lightsDetected, frame, base = ld.isColourFromFeedNonHistogram(frame,None,threshLvlLow,threshLvlHigh) #Charakterystyka kolorów 'pustego' widoku

	print("[Inf] Oczekuję na sygnały")

	while((len(frames[0]) + len(frames[1]) + len(frames[2])) < 3 * calibrationAccuracy and ret):
		ret, frame = cap.read()
		isLight, lightsDetected, frame, colours = ld.isColourFromFeedNonHistogram(frame,base,threshLvlLow,threshLvlHigh, 0.17, 0.17, 0.14)

		#Nadać sygnał do zapalenia modułów w kolorach (255,0,0) -> (0,255,0) ->
		#(0,0,255)

		if(isLight):
			if((lightsDetected[0]) and (not lightsDetected[1]) and (not lightsDetected[2])):
				if(len(frames[0]) < calibrationAccuracy):
					frames[0].append(frame)
			elif((not lightsDetected[0]) and (lightsDetected[1]) and (not lightsDetected[2])):
				if(len(frames[1]) < calibrationAccuracy):
					frames[1].append(frame)
			elif((not lightsDetected[0]) and (not lightsDetected[1]) and (lightsDetected[2])):
				if(len(frames[2]) < calibrationAccuracy):
					frames[2].append(frame)

		if(len(frames[0]) > 0):
			cv2.imshow("RED Frame Sample", cv2.resize(frames[0][0],(int(podglad[0] / 3), int(podglad[1] / 3))))
		if(len(frames[1]) > 0):
			cv2.imshow("GREEN Frame Sample", cv2.resize(frames[1][0],(int(podglad[0] / 3), int(podglad[1] / 3))))
		if(len(frames[2]) > 0):
			cv2.imshow("BLUE Frame Sample", cv2.resize(frames[2][0],(int(podglad[0] / 3), int(podglad[1] / 3))))

		cv2.imshow("Camera Feed", cv2.resize(frame,(podglad[0], podglad[1])))
		cv2.waitKey(1)
		#Dodać sprawdzenie, czy wszystkie klatki zostały osiągnięte

	cv2.destroyAllWindows() #SKALOWANIE

	print("[Inf] Otrzymywanie sygnałów zakończone")
	print("[Inf] Rozpoczynam kalibrację")
	#Wyciąga okna, które przekazały kod R -> G -> B
	error, windows = la.CalibrationBasic(calibrationAccuracy, frames, oknaX * oknaY,(((ih / oknaY) ** 2 + (iw / oknaX) ** 2) ** 0.5) / (6 ** 0.5),threshLvlLow,threshLvlHigh)
	print("[Inf] Różnica okien: ", error)
	print("[Inf] Usuwam artefakty")
	windowsCorners, usuniete = la.deleteArtifacts(windows,iw / (oknaX * 2), iw, ih)
	print("[Inf] Usuniętych artefaktów: ", usuniete)

	print("[Inf] Obliczam macierz skalowania")

	M = getWindowsPerspective(windowsCorners,iw,ih)
	print("[Inf] Macierz obliczona, przygotowuję obraz")

	frame0 = windowCornersPainter(frame0, windowsCorners) #Nanoszenie na obraz obramowań i środków znalezionych okien
	print("[Inf] Aby zamknąć okno naciśnij dowolny klawisz")
	#Prawdopodobnie NIE wszystkie okna zoztaną znalezione/oznaczone, nie
	#przejmowac się tym
	#Najważniejsze, żeby zaznaczone były narożniki
	
	cv2.imshow("Perspektywa", cv2.resize(cv2.warpPerspective(frame0,M,(iw,ih)),(podglad[0], podglad[1])))
	cv2.waitKey(0)
	cap.release()
	cv2.destroyAllWindows()

	print("[Inf] Przygotowuję macierz przeskalowanych okien")
	for w in range(len(frames[0])):
		frames[0][w] = cv2.warpPerspective(frames[0][w],M,(iw,ih))
		frames[1][w] = cv2.warpPerspective(frames[1][w],M,(iw,ih))
		frames[2][w] = cv2.warpPerspective(frames[2][w],M,(iw,ih))
	print("[Inf] Obliczam nową macierz")
	error, windows = la.CalibrationBasic(calibrationAccuracy, frames, oknaX * oknaY,(((ih / oknaY) ** 2 + (iw / oknaX) ** 2) ** 0.5) / (6 ** 0.5),threshLvlLow,threshLvlHigh)
	print("[Inf] Usuwam artefakty")
	windowsCorners, usuniete = la.deleteArtifacts(windows,iw / (oknaX * 2), iw, ih)
	print("[Inf] Usuniętych artefaktów: ", usuniete)
	frame1 = cv2.warpPerspective(frame1,M,(iw,ih))
	frame1 = windowCornersPainter(frame1, windowsCorners) #Nanoszenie na obraz obramowań i środków znalezionych okien
	print("[Inf] Aby zamknąć okno naciśnij dowolny klawisz")
	cv2.imshow("Final", cv2.resize(frame1,(podglad[0], podglad[1])))
	cv2.waitKey(0)


	cap.release()
	cv2.destroyAllWindows()

	print("[Inf] Kalibracja zakończona")
	return (windowsCorners, cv2.warpPerspective(framebasic,M,(iw,ih)), M)

def webCamInfo():
	cap = cv2.VideoCapture(0)
	ret, frame = cap.read()

	if(not ret):
		print("[Err] Nie wykryto kamerki lub jest źle skonfigurowana")
		return
	ih, iw, ic = frame.shape
	print("Rozmiar obrazu: ", iw, "x" , ih)
	print("Ilość kanałów koloru: ", ic)
	fps = cap.get(cv2.CAP_PROP_FPS)
	print("FPS: ", fps)

	cap.release()
	cv2.destroyAllWindows()

def getWindowsCorners(arrays, width, height):
	#Zamiana blobów na czworokąty
	#Okno =
	#<środek> [x,y]
	#<Narożniki> [[x,y],[x,y],[x,y],[x,y]]

	retTab = []
	for w in range(len(arrays)):
		pom0 = la.cornerSearchNP(arrays[w], width, height)[0]
		pom1 = la.centreNP(arrays[w])
		retTab.append([pom1, pom0])

	return retTab

def getWindowsPerspective(arrays, width, height):#w,h z kamery
	pom = []
	dConst = 2 * (((height / oknaY) ** 2 + (width / oknaX) ** 2) ** 0.5) / (6 ** 0.5)

	for w in range(len(arrays)):
		pom.append(arrays[w][0])

	corners, ids = la.cornerSearch(pom, width, height)

	corners[0][0] -= dConst
	corners[0][1] -= dConst
	corners[1][0] += dConst
	corners[1][1] -= dConst
	corners[2][0] += dConst
	corners[2][1] += dConst
	corners[3][0] -= dConst
	corners[3][1] += dConst

	pts1 = np.float32([[corners[0][0],corners[0][1]],[corners[1][0],corners[1][1]],[corners[2][0],corners[2][1]],[corners[3][0],corners[3][1]]]) #Do perspektywy
	pts2 = np.float32([[0,0],[width,0],[width,height],[0,height]]) #Widok domyślny
	M = cv2.getPerspectiveTransform(pts1,pts2)

	return M

def windowsAdder(windowsMatrix, basicFrame):
	if(windowsMatrix == None):
		inpt = input("[Warn] Nie ma wygenerowanej macierzy okien. Czy chcesz wszystko robić ręcznie? (T/n)")
		if(inpt != "T"): return
		else:
			if(dataFailureChecker()):
				windowsMatrix = []
				cap = cv2.VideoCapture(0)
				ret, BasicFrame = cap.read()
				cap.release()
				cv2.destroyAllWindows()
			else: return

	return(la.addMissingWindows(windowsMatrix,basicFrame,podglad[0],podglad[1]))

def windowsRemover(windowsMatrix, basicFrame):
	if(windowsMatrix == None):
		print("[Err] Brak okien do usunięcia. Użyj komendy 'calibrate' albo 'addwin'")
		return (None)

	return (la.deleteWindows(windowsMatrix,basicFrame,podglad[0],podglad[1]))

def camPrev(M):
	if(testBool):
		cap = cv2.VideoCapture(testPath)#Wideo
	else:
		cap = cv2.VideoCapture(0)#Webcam feed

	ret, frame = cap.read()
	if(not ret):
		print("[Err] Wystąpił problem z sygnałem")
		return

	ih, iw, ic = frame.shape
	isScaled = True
	if (M.all() == None):
		isScaled = False
		print("[Inf] Brak macierzy przekształcenia; dostępny jedynie nieprzetworzony obraz")

	while(ret):
		ret, frame = cap.read()
		if(not ret): break 
		cv2.imshow("Camera Feed", cv2.resize(frame,(podglad[0], podglad[1])))


		if(isScaled):
			cv2.imshow("Scaled Feed", cv2.resize(cv2.warpPerspective(frame,M,(iw,ih)),(podglad[0], podglad[1])))

		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	cap.release()
	cv2.destroyAllWindows()

def twoDimShow(array2d, basicFrame, M):
	if(testBool):
		cap = cv2.VideoCapture(testPath)#Wideo
	else:
		cap = cv2.VideoCapture(0)#Webcam feed

	ret, frame = cap.read()
	ih, iw, ic = frame.shape
	fps = cap.get(cv2.CAP_PROP_FPS)

	if(not ret):
		print("[Err] Brak sygnału wideo")
		return

	while(ret):
		ret, frame = cap.read()
		if(not ret):
			break
						
		frame = cv2.warpPerspective(frame,M,(iw,ih))
						
		for y in range(len(array2d)):
			for x in range(len(array2d[y])):
				frame = cv2.circle(frame,(array2d[y][x][0][0], array2d[y][x][0][1]),3,(255,255,255),1)
				frame = cv2.circle(frame,(array2d[y][x][1][0][0], array2d[y][x][1][0][1]),3,(0,0,255),1)
				frame = cv2.circle(frame,(array2d[y][x][1][1][0], array2d[y][x][1][1][1]),3,(0,0,255),1)
				frame = cv2.circle(frame,(array2d[y][x][1][2][0], array2d[y][x][1][2][1]),3,(0,0,255),1)
				frame = cv2.circle(frame,(array2d[y][x][1][3][0], array2d[y][x][1][3][1]),3,(0,0,255),1)
						
				frame = cv2.line(frame,(array2d[y][x][1][0][0], array2d[y][x][1][0][1]),(array2d[y][x][1][1][0], array2d[y][x][1][1][1]),(255,255,255))
				frame = cv2.line(frame,(array2d[y][x][1][1][0], array2d[y][x][1][1][1]),(array2d[y][x][1][2][0], array2d[y][x][1][2][1]),(255,255,255))
				frame = cv2.line(frame,(array2d[y][x][1][2][0], array2d[y][x][1][2][1]),(array2d[y][x][1][3][0], array2d[y][x][1][3][1]),(255,255,255))
				frame = cv2.line(frame,(array2d[y][x][1][0][0], array2d[y][x][1][0][1]),(array2d[y][x][1][3][0], array2d[y][x][1][3][1]),(255,255,255))
						
				frame = cv2.putText(frame,(str(x) + "#" + str(y)),(array2d[y][x][0][0], array2d[y][x][0][1]),cv2.FONT_HERSHEY_SIMPLEX,0.4,(255,255,255))
						
						
						
		cv2.imshow("Coordinated", cv2.resize(frame,(podglad[0], podglad[1])))
						
		if cv2.waitKey(int(1000 / fps)) & 0xFF == ord('q'):
			break
	cap.release()
	cv2.destroyAllWindows()

def windowCornersPainter(frame, windowsCorners):
	for w in range(len(windowsCorners)):
		frame = cv2.circle(frame,(windowsCorners[w][0][0], windowsCorners[w][0][1]),3,(255,255,255),1)
		frame = cv2.circle(frame,(windowsCorners[w][1][0][0], windowsCorners[w][1][0][1]),3,(0,0,255),1)
		frame = cv2.circle(frame,(windowsCorners[w][1][1][0], windowsCorners[w][1][1][1]),3,(0,0,255),1)
		frame = cv2.circle(frame,(windowsCorners[w][1][2][0], windowsCorners[w][1][2][1]),3,(0,0,255),1)
		frame = cv2.circle(frame,(windowsCorners[w][1][3][0], windowsCorners[w][1][3][1]),3,(0,0,255),1)

		frame = cv2.line(frame,(windowsCorners[w][1][0][0], windowsCorners[w][1][0][1]),(windowsCorners[w][1][1][0], windowsCorners[w][1][1][1]),(255,255,255))
		frame = cv2.line(frame,(windowsCorners[w][1][1][0], windowsCorners[w][1][1][1]),(windowsCorners[w][1][2][0], windowsCorners[w][1][2][1]),(255,255,255))
		frame = cv2.line(frame,(windowsCorners[w][1][2][0], windowsCorners[w][1][2][1]),(windowsCorners[w][1][3][0], windowsCorners[w][1][3][1]),(255,255,255))
		frame = cv2.line(frame,(windowsCorners[w][1][0][0], windowsCorners[w][1][0][1]),(windowsCorners[w][1][3][0], windowsCorners[w][1][3][1]),(255,255,255))
	return frame

def assignModulesToWindows(moduleList,windowList):
	assignList = []
	if(not testBool):
		if(len(moduleList) != oknaX * oknaY):
			print("[Err] Zła liczba zadeklarowanych okien, lub niepoprawna liczba modułów")
			print("Ilość dostarczonych modułów: ", len(moduleList))
			print("Ilość zadeklarowanych okien: ", oknaX * oknaY)
			return
		if(len(windowList) * len(windowList[0]) != oknaX*oknaY):
			print("[Err] Zła liczba okien, lub niepoprawna ilość wykrytych")
			print("Ilość wykrytych okien: ", len(windowList) * len(windowList[0]))
			print("Ilość zadeklarowanych okien: ", oknaX * oknaY)
			return

	if(testBool):
		cap = cv2.VideoCapture(testPath)#Wideo
	else:
		cap = cv2.VideoCapture(0)#Webcam feed

	ret, frame = cap.read()
	frame0 = copy.copy(frame)
	if(not ret):
		print("[Err] Brak wykrytego sygnału z kamery (wideo), przerywam")
		return

	ih, iw, ic = frame.shape
	ret, frame = cap.read()
	fps = cap.get(cv2.CAP_PROP_FPS)
	isLight, lightsDetected, frame, base = ld.isColourFromFeedNonHistogram(frame,None,threshLvlLow,threshLvlHigh) #Charakterystyka kolorów 'pustego' widoku
	dConst = (((ih / oknaY) ** 2 + (iw / oknaX) ** 2) ** 0.5) / (6 ** 0.5)

	print("[Inf] Oczekuję na sygnały")

	for i in range(len(moduleList)):
		frames = [[],[],[]] #RGB

		#KomendaDoZapaleniaModułuZListy(moduleList[i])
		while((len(frames[0]) + len(frames[1]) + len(frames[2])) < 3 * calibrationAccuracy and ret):
			ret, frame = cap.read()
			isLight, lightsDetected, frame, colours = ld.isColourFromFeedNonHistogram(frame,base,threshLvlLow,threshLvlHigh, 0.17, 0.17, 0.14)
			frame = cv2.warpPerspective(frame,M,(iw,ih))

			if(isLight):
				if((lightsDetected[0]) and (not lightsDetected[1]) and (not lightsDetected[2])):
					if(len(frames[0]) < calibrationAccuracy):
						frames[0].append(frame)
				elif((not lightsDetected[0]) and (lightsDetected[1]) and (not lightsDetected[2])):
					if(len(frames[1]) < calibrationAccuracy):
						frames[1].append(frame)
				elif((not lightsDetected[0]) and (not lightsDetected[1]) and (lightsDetected[2])):
					if(len(frames[2]) < calibrationAccuracy):
						frames[2].append(frame)
						
				for y in range(len(windowList)):
					for x in range(len(windowList[y])):
						frame = cv2.circle(frame,(windowList[y][x][0][0], windowList[y][x][0][1]),3,(255,255,255),1)
						frame = cv2.circle(frame,(windowList[y][x][1][0][0], windowList[y][x][1][0][1]),3,(0,0,255),1)
						frame = cv2.circle(frame,(windowList[y][x][1][1][0], windowList[y][x][1][1][1]),3,(0,0,255),1)
						frame = cv2.circle(frame,(windowList[y][x][1][2][0], windowList[y][x][1][2][1]),3,(0,0,255),1)
						frame = cv2.circle(frame,(windowList[y][x][1][3][0], windowList[y][x][1][3][1]),3,(0,0,255),1)
						
						frame = cv2.line(frame,(windowList[y][x][1][0][0], windowList[y][x][1][0][1]),(windowList[y][x][1][1][0], windowList[y][x][1][1][1]),(255,255,255))
						frame = cv2.line(frame,(windowList[y][x][1][1][0], windowList[y][x][1][1][1]),(windowList[y][x][1][2][0], windowList[y][x][1][2][1]),(255,255,255))
						frame = cv2.line(frame,(windowList[y][x][1][2][0], windowList[y][x][1][2][1]),(windowList[y][x][1][3][0], windowList[y][x][1][3][1]),(255,255,255))
						frame = cv2.line(frame,(windowList[y][x][1][0][0], windowList[y][x][1][0][1]),(windowList[y][x][1][3][0], windowList[y][x][1][3][1]),(255,255,255))
						
						frame = cv2.putText(frame,(str(x) + "#" + str(y)),(windowList[y][x][0][0], windowList[y][x][0][1]),cv2.FONT_HERSHEY_SIMPLEX,0.4,(255,255,255))

				cv2.imshow("Live Feed", cv2.resize(frame,(podglad[0], podglad[1])))
				cv2.waitKey(int(1000 / fps))


		cv2.destroyAllWindows() #SKALOWANIE
		#Wyciąga okna, które przekazały kod R -> G -> B
		error, windows = la.CalibrationBasic(calibrationAccuracy, frames, oknaX * oknaY, dConst, threshLvlLow,threshLvlHigh)
		if(len(windows) == 1):
			singleWindow = [la.centreNP(windows[0]), la.cornerSearchNP(windows[0], iw, ih)[0]]
		else:
			print("[Err] Coś się zepsuło i nie było mnie widać")
			break

		min = iw*ih
		id = [-1, -1]
		certainty = 0
		for y in range(len(windowList)):
			for x in range(len(windowList[y])):
				distance = ((singleWindow[0][0] - windowList[y][x][0][0])**2 + (singleWindow[0][1] - windowList[y][x][0][1])**2)**0.5
				if(distance < min and distance < dConst):
					id = [x,y]
					min = distance
					certainty = round(100 - ((distance/dConst) * 100),2)
		if(id[0] != -1):
			assignList.append([id[0],id[1],certainty])
			print("[Inf] Moduł o ID ", len(assignList)-1, " posiada koordynaty X: ", id[0], ", Y: ", id[1], ". Pewność przypisania: ", certainty, "%")
		else:
			assignList.append(None)

	return assignList

print("#   #  ##   ##     #     ##   ###")
print("## ## #  # #       #    #     #  #")
print("# # # #  #  ##     #     ##   #  #")
print("#   # #  #    #    #       #  #  #")
print("#   #  ##   ##     ###   ##   ###")
print("                   Light Show Debug")
print()

oknaX = 1
oknaY = 1
podglad = [1028,720]
calibrationAccuracy = 1
testPath = "test.mp4"
testBool = False
threshLvlLow = 180
threshLvlHigh = 255
windowsMatrix = None
basicFrame = None
twoDim = None
RXList = None#Zdobyć listę modułów
AssignedList = None
						
#0 1
#3 2
pts1 = np.float32([[0,0],[1,0],[1,1],[0,1]]) #Do perspektywy
pts2 = np.float32([[0,0],[1,0],[1,1],[0,1]]) #Widok domyślny
M = None
						   
#oknaX = 12
#oknaY = 10
#calibrationAccuracy = 1
#testPath = "LSD_Test_01.mp4"
#testBool = True
#windowsMatrix, basicFrame, M = calibrationFramesExtraction()
						
						
#Wiersz poleceń
loop = True
while(loop):
	inp = input("\nPolecenie: ")
	if(inp == "help"):
		print("Lista poleceń: ")
		print("quit - wyjście")
		print("winsize - rozmiar okna podglądu graficznego")
		print("builwin - rozmiary budynku (ilość okien)")
		print("calibrate - dostosowuję program do widoku budynku (z kamery)")
		print("addwin - dodawanie okien nie wykrytych przez program (musi być wcześniej calibrate!)")
		print("delwin - usuwanie okien niepoprawnie wykrytych przez program (musi być wcześniej calibrate!)")
		print("coorwin - oblicza koordynaty okien (przygotowanie do obliczania pozycji pojedynczych okien)")
		print("asswin - przypisuje okna do ID Modułów")
		print("calibrateAcc - Ilość klatek wideo do kalibracji (nie zalecane więcej, niż 10)")
		print("webcaminfo - sprawdza działanie kamerki, pokazuje o niej informacje")
		print("webcamfeed - obraz na żywo z kamerki")
		print("testenable - wprowadza program w tryb testowy (feed z pliku wideo)")
		print("testdisable - wprowadza program w tryb normalny (feed z kamerki)")
		print("threshlow - niski próg wykrywania światła (lepiej nie zmieniać, jeżeli nie wiesz, co robisz)")
		print("threshhigh - wysoki próg wykrywania światła (lepiej nie zmieniać, jeżeli nie wiesz, co robisz)")
	elif(inp == "quit"):   
		loop = False       
	elif(inp == "winsize"):
		podglad[0] = int(input("Szerokość okna podglądu (px): "))
		podglad[1] = int(input("Wysokość okna podglądu (px): "))
	elif(inp == "builwin"):
		oknaX = int(input("Ilość kolumn okien w budynku: "))
		oknaY = int(input("Ilość rzędów okien w budynku: "))
	elif(inp == "calibrate"):
		if(dataFailureChecker()):
			windowsMatrix, basicFrame, M = calibrationFramesExtraction()
	elif(inp == "addwin"): 
		windowsMatrix = windowsAdder(windowsMatrix, basicFrame)
	elif(inp == "delwin"): 
		windowsMatrix = windowsRemover(windowsMatrix, basicFrame)
	elif(inp == "coorwin"):
		if(windowsMatrix != None):
			twoDim = la.arrayTo2DArray(windowsMatrix, oknaX, oknaY)
			twoDimShow(twoDim,basicFrame,M)
		else:
			print("[Err] Brak macierzy okien")
	elif(inp == "asswin"):
		AssignedList = assignModulesToWindows([0],twoDim)#Dodać listę okien
	elif(inp == "korwin"):
		print("Blisko ale nie do końca, pewnie szukasz komendy 'coorwin' ;)")
	elif(inp == "calibrateAcc"):
		calibrationAccuracy = int(input("Podaj dokładność kalibracji (domyślne 1): "))
	elif(inp == "webcaminfo"):
		webCamInfo()
	elif(inp == "testenable"):
		testPath = input("Podaj nazwę pliku (nazwa.format): ")
		testBool = True
	elif(inp == "testdisable"):
		print("[Inf] Tryb normalny włączony")
		testBool = False
	elif(inp == "threshlow"):
		threshLvlLow = int(input("Thresh poziom niski: "))
	elif(inp == "threshhigh"):
		threshLvlLow = int(input("Thresh poziom wysoki: "))
	elif(inp == "webcamfeed"):
		camPrev(M)
	else:
		print("[Err] Polecenie niepoprawne. Wpisz 'help' aby otrzymać listę poleceń")