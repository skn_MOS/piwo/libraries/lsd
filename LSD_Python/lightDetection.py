from imutils.perspective import four_point_transform
from imutils import contours
from skimage import measure
import lightAssign as la
import numpy as np
import imutils
import cv2
import math
import copy

maxHeight = 384
maxWidth = 384
maxSamplingHor = 256
maxSamplingVer = 384

def HistMax(hist):
	max = 1
	for w in range(len(hist[0])):
		if(hist[0][w] > max): max = hist[0][w]
		if(hist[1][w] > max): max = hist[1][w]
		if(hist[2][w] > max): max = hist[2][w]
	return max

def getPXCol(frame):
	bgr_planes = cv2.split(frame)
	histSize = 256
	histRange = (0, 256)
	accumulate = False
	b_hist = cv2.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)
	g_hist = cv2.calcHist(bgr_planes, [1], None, [histSize], histRange, accumulate=accumulate)
	r_hist = cv2.calcHist(bgr_planes, [2], None, [histSize], histRange, accumulate=accumulate)
	hist_w = 512
	hist_h = 400
	bin_w = int(round( hist_w/histSize ))
	histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)
	cv2.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
	cv2.normalize(g_hist, g_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
	cv2.normalize(r_hist, r_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)

	return ([r_hist,g_hist,b_hist])

def cvShowHist(hist):
	frame = np.zeros((maxWidth,maxHeight,3),dtype='uint8')

	max = HistMax(hist)

	if(max != 1): max = max[0]
	lineStartR = [0,maxHeight]
	lineStartG = [0,maxHeight]
	lineStartB = [0,maxHeight]
	for w in range(maxSamplingHor):

		frame = cv2.line(frame,(lineStartR[0],lineStartR[1]),(int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[0][w][0]/max) * maxHeight))),(0,0,255))
		frame = cv2.line(frame,(lineStartG[0],lineStartG[1]),(int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[1][w][0]/max) * maxHeight))),(0,255,0))
		frame = cv2.line(frame,(lineStartB[0],lineStartB[1]),(int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[2][w][0]/max) * maxHeight))),(255,0,0))

		lineStartR = [int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[0][w]/max) * maxHeight))]
		lineStartG = [int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[1][w]/max) * maxHeight))]
		lineStartB = [int((w+1) * (maxWidth/maxSamplingHor)),int(maxHeight - ((hist[2][w]/max) * maxHeight))]

	cv2.imshow("Hist",frame)
	cv2.waitKey(1)

def isColourFromFeed(frame, histogram, threshold):
	#Pierwsze uruchomienie, lub bez wykrywania, histogram = None

	if(histogram == None):
		return (False, [False, False, False], frame, getPXCol(frame))
	else:
		#Interesujące są tylko najjaśniejsze punkty; końcówka histogramu
		end = int(256/4)
		
		overall = False
		red = False
		green = False
		blue = False

		maxv = 0
		maxid = -1

		localHist = getPXCol(frame)

		for w in range(256-end,256):
			if((localHist[0][w] - histogram[0][w]) > threshold):
				overall = True
				red = True

			if((localHist[1][w] - histogram[1][w]) > threshold):
				overall = True
				green = True

			if((localHist[2][w] - histogram[2][w]) > threshold):
				overall = True
				blue = True

		#if(maxid != -1):
		#	overall = True
		#	if(maxid == 0): red = True
		#	elif(maxid == 1): green = True
		#	elif(maxid == 2): blue = True

	#Return True/False, colour RGB [bool,bool,bool], frame, histograms
	return (overall, [red,green,blue] , frame, localHist)

def isColourFromFeedNonHistogram(frame, baseMat, thLow, thHigh, sensR = 1, sensG = 1, sensB = 1):
	base = []
	base.append(la.extract(frame,(0,0,125),(5,5,255),thLow,thHigh)[0])
	base.append(la.extract(frame,(0,128,0),(16,255,16),thLow,thHigh)[0])
	base.append(la.extract(frame,(128,0,0),(255,16,16),thLow,thHigh)[0])

	avg_color_per_row_R = np.average(base[0], axis=0)
	avg_color_R = np.average(avg_color_per_row_R, axis=0)

	avg_color_per_row_G = np.average(base[1], axis=0)
	avg_color_G = np.average(avg_color_per_row_G, axis=0)

	avg_color_per_row_B = np.average(base[2], axis=0)
	avg_color_B = np.average(avg_color_per_row_B, axis=0)

	colorMat = [avg_color_R, avg_color_G, avg_color_B]

	if(baseMat == None):
		return (False, [False,False,False], frame, colorMat)

	overall = False
	red = False
	green = False
	blue = False

	if(colorMat[0] - baseMat[0] > sensR):
		overall = True
		red = True
	if(colorMat[1] - baseMat[1] > sensG):
		overall = True
		green = True
	if(colorMat[2] - baseMat[2] > sensB):
		overall = True
		blue = True

	return (overall, [red, green, blue], frame, colorMat)