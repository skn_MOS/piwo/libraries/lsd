#Light Source Detector

from imutils.perspective import four_point_transform
from imutils import contours
from skimage import measure
import numpy as np
import imutils
import cv2
import math
import copy

ph_NULL = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_null.png")
ph_m_m = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_m_m.png")
ph_0_1 = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_0_1.png")
#ph_2_1 = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_3-zdjecia/Photos/akademik_null.png")
ph_5_8 = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_5_8.png")
ph_11_2 = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_11_2.png")
ph_m_m_R = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_m_m_R.png")
ph_m_m_G = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_m_m_G.png")
ph_m_m_B = cv2.imread("C:/Users/tq481/source/repos/PIWO_LSD_Coloured/Photos/akademik_m_m_B.png")

oknaX = 12
oknaY = 10
frame = ph_NULL
height = 988 
width = 1292

outWinSiz = [1000,800]

refPt = []
selection = False

przeksztalcenie = np.zeros((height,width,3), dtype='uint8')

def extract(frame, colour_low, colour_high):#wyciąga kolor ze zdjęcia
    image = cv2.blur(frame,(2,2))#Filtry pierwszego stopnia
    blurred = cv2.GaussianBlur(image, (5,5), 0)
    blurred = cv2.GaussianBlur(blurred, (5,5), 0)
    thresh = cv2.threshold(blurred, 125, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.GaussianBlur(thresh, (5,5), 0)
    thresh = cv2.GaussianBlur(thresh, (5,5), 0)
    thresh = cv2.erode(thresh, None, iterations=1)
    thresh = cv2.dilate(thresh, None, iterations=2)

    #Odcinanie kolorów poniżej podanego stopnia
    lower = np.array(colour_low)  #-- Niski zakres kolorów-- 
    upper = np.array(colour_high)  #-- Wysoki zakres --
    mask = cv2.inRange(thresh, lower, upper)
    res = cv2.bitwise_and(thresh, thresh, mask= mask)

    #Filtry drugiego stopnia
    res = cv2.GaussianBlur(res, (5,5), 0)
    res = cv2.threshold(res, 155, 255, cv2.THRESH_BINARY)[1]
    res = cv2.erode(res, None, iterations=1)
    res = cv2.dilate(res, None, iterations=2)

    #Filtry trzeciego stopnia
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5,5), 0)
    gray = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)[1]

    #Szukanie okien(Prostokątów)
    cnts = cv2.findContours(gray.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    docCnt = None

    #Zwraca czarno-białe zdjęcie; tablicę z punktami z obwodów okien
    #np.array(y,x,3)
    #tablica[[[x,y],[x,y],[x,y]],[[x,y],[x,y],[x,y]],[[x,y],[x,y],[x,y]]...]
    return (gray,cnts)

def CalibrationBasic(accuracy):#accuracy (ilość odtwarzanych sekwencji RGB), Zwraca kod błędu(0-brak), tablicę okien
    if accuracy <= 0: return -1
    dConst = (((height/oknaY)**2 + (width/oknaX)**2)**0.5)/(6**0.5)#Stała odległościowa, jeżeli okna są
    #Dalej niż ta stała, NIE są uznawane za leżące w tym samym miejscu
    roznicaOkien = 0
    error = 0
    #0 - znalezione okna i ilość okien się zgadza
    #1$$ - znaleziono więcej okien, $$-ilość nadwyżki
    #2$$ - znaleziono mniej okien, $$- ilość deficytu
    #3 - nie wiem, co to oznacza, jak się pojawi, to coś się zjebało

    #windowsListMerged = np.zeros((oknaY,oknaX,5), dtype='uint8')#Nieposegregowane
    windowsListMerged = []
    for w in range(accuracy):#Podpiąć pod Feed z kamery
        EXR = extract(ph_m_m_R,(0,0,125),(5,5,255))
        EXG = extract(ph_m_m_G,(0,128,0),(16,255,16))
        EXB = extract(ph_m_m_B,(128,0,0),(255,16,16))

        pointsListRed = EXR[1]
        pointsListGreen = EXG[1]
        pointsListBlue = EXB[1]

        windowsListRed = EXR[1]
        windowsListGreen = EXG[1]
        windowsListBlue = EXB[1]

        worklist = []
        worklistFull = []
        centersListPom = []

        for r in range(len(windowsListRed)):
            print("R:", r, "/", len(windowsListRed))
            min = width*height
            id = [-1,-1]
            coorRed = centreNP(windowsListRed[r])
            for g in range(len(windowsListGreen)):
                coorGreen = centreNP(windowsListGreen[g])
                dVar = ((coorRed[0] - coorGreen[0])**2 + (coorRed[1] - coorGreen[1])**2)**0.5
                if (dVar < dConst):
                    if(dVar < min):
                        min = dVar
                        id = [r,g]
                        centersListPom.append([(coorRed[0]+coorGreen[0])/2,(coorRed[1]+coorGreen[1])/2])
            if(id[0] != -1): worklist.append(id)

        centresList = np.zeros((len(worklist),2))
        for e in range(len(worklist)):
            centresList[e][0] = centersListPom[e][0]
            centresList[e][1] = centersListPom[e][1]

        for rg in range(len(worklist)):
            print("RG:", rg, "/", len(worklist))
            min = width*height
            id = [-1,-1,-1]
            coorRedGreen = centresList[rg]
            for b in range(len(windowsListBlue)):
                coorBlue = centreNP(windowsListBlue[b])
                dVar = ((coorRedGreen[0] - coorBlue[0])**2 + (coorRedGreen[1] - coorBlue[1])**2)**0.5
                if (dVar < dConst):
                    if (dVar < min):
                        min = dVar
                        id = [worklist[rg][0],worklist[rg][1],b]
            if(id[0] != -1): worklistFull.append(id)#Mamy pokrywające się okna


        roznicaOkien = oknaX*oknaY - len(worklistFull)

        licznik = 0
        if(roznicaOkien < 0):
            roznicaOkienTemp = roznicaOkien*(-1)
            licznik = 2
            while(roznicaOkienTemp >= 1):
                licznik *= 10
                roznicaOkienTemp /= 10
            licznik += roznicaOkien*(-1)

        if(roznicaOkien > 0):
            roznicaOkienTemp = roznicaOkien
            licznik = 1
            while(roznicaOkienTemp >= 1):
                licznik *= 10
                roznicaOkienTemp /= 10
            licznik += roznicaOkien

        error = licznik
        
        for q in range(len(worklistFull)):
            pom = []
            centreX = (centreNP(pointsListRed[worklistFull[q][0]])[0] + centreNP(pointsListGreen[worklistFull[q][1]])[0] + centreNP(pointsListBlue[worklistFull[q][2]])[0])/3
            centreY = (centreNP(pointsListRed[worklistFull[q][0]])[1] + centreNP(pointsListGreen[worklistFull[q][1]])[1] + centreNP(pointsListBlue[worklistFull[q][2]])[1])/3
            pom.append([centreX, centreY])

            obwod = []
            for w in pointsListRed[worklistFull[q][0]]: obwod.append(w)
            for w in pointsListGreen[worklistFull[q][1]]: obwod.append(w)
            for w in pointsListBlue[worklistFull[q][2]]: obwod.append(w)

            narozniki = cornerSearchNP(obwod)
            for e in range(4):
                if(narozniki != -1): pom.append(narozniki[0][e])
                else:
                   pom = []
                   break

            if(len(pom) != 0): windowsListMerged.append(pom)

    return (error,windowsListMerged)

def CalibrationBasic2(accuracy):#accuracy (ilość odtwarzanych sekwencji RGB), Zwraca kod błędu(0-brak), tablicę okien
    if accuracy <= 0: return -1
    dConst = (((height/oknaY)**2 + (width/oknaX)**2)**0.5)/(6**0.5)#Stała odległościowa, jeżeli okna są
    #Dalej niż ta stała, NIE są uznawane za leżące w tym samym miejscu
    roznicaOkien = 0
    error = 0
    #0 - znalezione okna i ilość okien się zgadza
    #1$$ - znaleziono więcej okien, $$-ilość nadwyżki
    #2$$ - znaleziono mniej okien, $$- ilość deficytu
    #3 - nie wiem, co to oznacza, jak się pojawi, to coś się zjebało

    #windowsListMerged = np.zeros((oknaY,oknaX,5), dtype='uint8')#Nieposegregowane
    windowsListMerged = []
    for dok in range(accuracy):#Podpiąć pod Feed z kamery
        EXR = extract(ph_m_m_R,(0,0,125),(5,5,255))
        EXG = extract(ph_m_m_G,(0,128,0),(16,255,16))
        EXB = extract(ph_m_m_B,(128,0,0),(255,16,16))

        pointsListRed = EXR[1]
        pointsListGreen = EXG[1]
        pointsListBlue = EXB[1]

        cornersListRed = []
        cornersListGreen = []
        cornersListBlue = []

        for w in pointsListRed:
            Pom = []
            Pom.append(centreNP(w))
            Pom.append(cornerSearchNP(w)[0])
            cornersListRed.append(Pom)

        for w in pointsListGreen:
            Pom = []
            Pom.append(centreNP(w))
            Pom.append(cornerSearchNP(w)[0])
            cornersListGreen.append(Pom)

        for w in pointsListBlue:
            Pom = []
            Pom.append(centreNP(w))
            Pom.append(cornerSearchNP(w)[0])
            cornersListBlue.append(Pom)

        worklist = []
        for r in range(len(cornersListRed)):
            min = width*height
            id = [-1,-1]
            for g in range(len(cornersListGreen)):
                dVar = ((cornersListRed[r][0][0] - cornersListGreen[g][0][0])**2 + (cornersListRed[r][0][1] - cornersListGreen[g][0][1])**2)**0.5
                if(dVar < dConst):
                    if(dVar < min):
                        min = dVar
                        id = [r,g]
            if(id[0] != -1): worklist.append(id)

        worklistRGB = []
        for rg in range(len(worklist)):
            min = width*height
            id = [-1,-1,-1]
            for b in range(len(cornersListBlue)):
                dVar = (((cornersListRed[worklist[rg][0]][0][0]+cornersListGreen[worklist[rg][1]][0][0])/2 - cornersListBlue[b][0][0])**2 +
                       ((cornersListRed[worklist[rg][0]][0][1]+cornersListGreen[worklist[rg][1]][0][1])/2 - cornersListBlue[b][0][1])**2)**0.5
                if(dVar < dConst):
                    if(dVar < min):
                        min = dVar
                        id = [worklist[rg][0],worklist[rg][1],b]
            if(id[0] != -1): worklistRGB.append(id)


        roznicaOkien = oknaX*oknaY - len(worklistRGB)

        licznik = 0
        if(roznicaOkien < 0):
            roznicaOkienTemp = roznicaOkien*(-1)
            licznik = 2
            while(roznicaOkienTemp >= 1):
                licznik *= 10
                roznicaOkienTemp /= 10
            licznik += roznicaOkien*(-1)

        if(roznicaOkien > 0):
            roznicaOkienTemp = roznicaOkien
            licznik = 1
            while(roznicaOkienTemp >= 1):
                licznik *= 10
                roznicaOkienTemp /= 10
            licznik += roznicaOkien

        error = licznik

        for w in range(len(worklistRGB)):
            pom = []
            for r in range(4): pom.append(cornersListRed[worklistRGB[w][0]][1][r])
            for r in range(4): pom.append(cornersListGreen[worklistRGB[w][1]][1][r])
            for r in range(4): pom.append(cornersListBlue[worklistRGB[w][2]][1][r])
            pom = cornerSearch(pom)
            ctr = [centre(pom[0])]
            ctr.append(pom[0])
            windowsListMerged.append(ctr)

    return (error,windowsListMerged)

def centreNP(tablica):
    x = 0
    y = 0
    for w in range(len(tablica)):
        x += tablica[w][0][0]
        y += tablica[w][0][1]
    x = int(x/len(tablica))
    y = int(y/len(tablica))
    return ([x,y])

def centre(tablica):
    x = 0
    y = 0
    for w in range(len(tablica)):
        x += tablica[w][0]
        y += tablica[w][1]
    x = int(x/len(tablica))
    y = int(y/len(tablica))
    return ([x,y])

def cornerSearchNP(tablica):#Szuka narożników dla tablic NumPy Array
    if(len(tablica) < 4): return (-1)

    #0   1
    #3   2

    narozniki = []
    tab = []
    for w in range(4): narozniki.append([tablica[w][0][0],tablica[w][0][1]])
    for w in range(len(tablica)): tab.append([tablica[w][0][0],tablica[w][0][1]])
    mapCorner = [[0,0],[width,0],[width,height],[0,height]]
    distance = [((tab[0][0] - mapCorner[0][0])**2 + (tab[0][1] - mapCorner[0][1])**2)**0.5,
                ((tab[1][0] - mapCorner[1][0])**2 + (tab[1][1] - mapCorner[1][1])**2)**0.5,
                ((tab[2][0] - mapCorner[2][0])**2 + (tab[2][1] - mapCorner[2][1])**2)**0.5,
                ((tab[3][0] - mapCorner[3][0])**2 + (tab[3][1] - mapCorner[3][1])**2)**0.5]
    id = [0,0,0,0]

    for w in range(1,len(tablica)):
        if (distance[0] > ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5):
            distance[0] = ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5
            narozniki[0][0] = tab[w][0]
            narozniki[0][1] = tab[w][1]
            id[0] = w
        if (distance[1] > ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5):
            distance[1] = ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5
            narozniki[1][0] = tab[w][0]
            narozniki[1][1] = tab[w][1]
            id[1] = w
        if (distance[2] > ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5):
            distance[2] = ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5
            narozniki[2][0] = tab[w][0]
            narozniki[2][1] = tab[w][1]
            id[2] = w
        if (distance[3] > ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5):
            distance[3] = ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5
            narozniki[3][0] = tab[w][0]
            narozniki[3][1] = tab[w][1]
            id[3] = w
    return(narozniki,id)

def cornerSearch(tablica):#Robi to samo, tylko dla zwyczajnych tablic
    if(len(tablica) < 4): return (-1)

    #0   1
    #3   2

    narozniki = []
    tab = []
    for w in range(4): narozniki.append([tablica[w][0],tablica[w][1]])
    for w in range(len(tablica)): tab.append([tablica[w][0],tablica[w][1]])
    mapCorner = [[0,0],[width,0],[width,height],[0,height]]
    distance = [((tab[0][0] - mapCorner[0][0])**2 + (tab[0][1] - mapCorner[0][1])**2)**0.5,
                ((tab[1][0] - mapCorner[1][0])**2 + (tab[1][1] - mapCorner[1][1])**2)**0.5,
                ((tab[2][0] - mapCorner[2][0])**2 + (tab[2][1] - mapCorner[2][1])**2)**0.5,
                ((tab[3][0] - mapCorner[3][0])**2 + (tab[3][1] - mapCorner[3][1])**2)**0.5]
    id = [0,0,0,0]

    for w in range(1,len(tablica)):
        if (distance[0] > ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5):
            distance[0] = ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5
            narozniki[0][0] = tab[w][0]
            narozniki[0][1] = tab[w][1]
            id[0] = w
        if (distance[1] > ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5):
            distance[1] = ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5
            narozniki[1][0] = tab[w][0]
            narozniki[1][1] = tab[w][1]
            id[1] = w
        if (distance[2] > ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5):
            distance[2] = ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5
            narozniki[2][0] = tab[w][0]
            narozniki[2][1] = tab[w][1]
            id[2] = w
        if (distance[3] > ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5):
            distance[3] = ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5
            narozniki[3][0] = tab[w][0]
            narozniki[3][1] = tab[w][1]
            id[3] = w
    return(narozniki,id)

def cornerSearch2NP(tablica):
    tab = []
    for w in range(len(tablica)): tab.append([tablica[w][0][0],tablica[w][0][1]])
    furthestID = [-1,-1]
    min = 0
    for w in range(len(tab)):
        for e in range(len(tab)):
            if(w!=e):
                dVar = ((tab[w][0]-tab[e][0])**2 + (tab[w][1]-tab[e][1])**2)**0.5
                if(dVar > min):
                    min = dVar
                    furthestID = [w,e]

    prosta = [0,0]
    if(tab[furthestID[0]][0] == tab[furthestID[1]][0]): tab[furthestID[0]][0]+= 0.002

    prosta[0] = (tab[furthestID[0]][1]-tab[furthestID[1]][1])/(tab[furthestID[0]][0]-tab[furthestID[1]][0])
    prosta[1] = tab[furthestID[0]][1] - (tab[furthestID[0]][0]*prosta[0])

    min = 0
    max = 0
    id0 = -1
    id1 = -1

    for w in range(len(tab)):
        y0 = (tab[w][0]*prosta[0] + prosta[1])
        y1 = tab[w][1]
        if(y0-y1 < min):
            min = y0-y1
            id0 = w
        elif(y0-y1 > max):
            max = y0-y1
            id1 = w

    ids = [furthestID[0],furthestID[1],id0,id1]
    narozniki = []

    narozniki.append(tab[furthestID[0]])
    narozniki.append(tab[furthestID[1]])
    narozniki.append(tab[id0])
    narozniki.append(tab[id1])

    pom = cornerSearch(narozniki)
    narozniki = pom[0]
    id = [ids[pom[1][0]],ids[pom[1][1]],ids[pom[1][2]],ids[pom[1][3]]]

    return(narozniki,id)

def cornerSearch2(tab):

    furthestID = [-1,-1]
    min = 0
    for w in range(len(tab)):
        for e in range(len(tab)):
            if(w!=e):
                dVar = ((tab[w][0]-tab[e][0])**2 + (tab[w][1]-tab[e][1])**2)**0.5
                if(dVar > min):
                    min = dVar
                    furthestID = [w,e]

    prosta = [0,0]
    if(tab[furthestID[0]][0] == tab[furthestID[1]][0]): tab[furthestID[0]][0]+= 0.002

    prosta[0] = (tab[furthestID[0]][1]-tab[furthestID[1]][1])/(tab[furthestID[0]][0]-tab[furthestID[1]][0])
    prosta[1] = tab[furthestID[0]][1] - (tab[furthestID[0]][0]*prosta[0])

    min = 0
    max = 0
    id0 = -1
    id1 = -1

    for w in range(len(tab)):
        y0 = (tab[w][0]*prosta[0] + prosta[1])
        y1 = tab[w][1]
        if(y0-y1 < min):
            min = y0-y1
            id0 = w
        elif(y0-y1 > max):
            max = y0-y1
            id1 = w

    ids = [furthestID[0],furthestID[1],id0,id1]
    narozniki = []

    narozniki.append(tab[furthestID[0]])
    narozniki.append(tab[furthestID[1]])
    narozniki.append(tab[id0])
    narozniki.append(tab[id1])

    pom = cornerSearch(narozniki)
    narozniki = pom[0]
    id = [ids[pom[1][0]],ids[pom[1][1]],ids[pom[1][2]],ids[pom[1][3]]]

    return(narozniki,id)

def photoshow(FRed,FGreen,FBlue):
    #Czerwony
    frame = FRed
    cl = [0,0,125]
    ch = [5,5,255]

    out = extract(frame,cl,ch)
    photo = out[0]
    photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

    for w in range(len(out[1])):
        midX = 0
        midY = 0
        for e in range(len(out[1][w])):
            tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
            midX += tupla[0]
            midY += tupla[1]
            cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
        midX /= len(out[1][w])
        midY /= len(out[1][w])
        cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(16,32,255),2)
        nrz = cornerSearchNP(out[1][w])
        if(nrz != -1):
            cv2.circle(photo, (nrz[0][0][0],nrz[0][0][1]), 3, (0, 0, 255), 3)
            cv2.circle(photo, (nrz[0][1][0],nrz[0][1][1]), 3, (0, 255, 0), 3)
            cv2.circle(photo, (nrz[0][2][0],nrz[0][2][1]), 3, (255, 0, 0), 3)
            cv2.circle(photo, (nrz[0][3][0],nrz[0][3][1]), 3, (0, 255, 255), 3)

            cv2.line(photo,(nrz[0][0][0],nrz[0][0][1]),(nrz[0][1][0],nrz[0][1][1]),(255,255,0),2)
            cv2.line(photo,(nrz[0][1][0],nrz[0][1][1]),(nrz[0][2][0],nrz[0][2][1]),(255,255,0),2)
            cv2.line(photo,(nrz[0][2][0],nrz[0][2][1]),(nrz[0][3][0],nrz[0][3][1]),(255,255,0),2)
            cv2.line(photo,(nrz[0][0][0],nrz[0][0][1]),(nrz[0][3][0],nrz[0][3][1]),(255,255,0),2)

    cv2.imshow('Red',cv2.resize(photo,(outWinSiz[0],outWinSiz[1])))

    #Zielony
    frame = FGreen
    cl = [0,125,0]
    ch = [5,255,5]

    out = extract(frame,cl,ch)
    photo = out[0]
    photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

    for w in range(len(out[1])):
        midX = 0
        midY = 0
        for e in range(len(out[1][w])):
            tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
            midX += tupla[0]
            midY += tupla[1]
            cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
        midX /= len(out[1][w])
        midY /= len(out[1][w])
        cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(32,255,16),2)

    cv2.imshow('Green',cv2.resize(photo,(outWinSiz[0],outWinSiz[1])))

    #Niebieski
    frame = FBlue
    cl = [125,0,0]
    ch = [255,50,5]

    out = extract(frame,cl,ch)
    photo = out[0]
    photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

    for w in range(len(out[1])):
        midX = 0
        midY = 0
        for e in range(len(out[1][w])):
            tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
            midX += tupla[0]
            midY += tupla[1]
            cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
        midX /= len(out[1][w])
        midY /= len(out[1][w])
        cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,16,32),2)

    cv2.imshow('Blue',cv2.resize(photo,(outWinSiz[0],outWinSiz[1])))

def deleteArtifacts(tab,accuracy = width/(oknaX*2)):
    wynik = []
    average0 = 0
    average1 = 0

    for w in range(len(tab)):
        average0 += ((tab[w][1][0][0] - tab[w][1][2][0])**2 + (tab[w][1][0][1] - tab[w][1][2][1])**2)**0.5
        average1 += ((tab[w][1][1][0] - tab[w][1][3][0])**2 + (tab[w][1][1][1] - tab[w][1][3][1])**2)**0.5
    average0 /= len(tab)
    average1 /= len(tab)

    for w in range(len(tab)):
        if(abs(((tab[w][1][0][0] - tab[w][1][2][0])**2 + (tab[w][1][0][1] - tab[w][1][2][1])**2)**0.5 - average0) < accuracy):
            if(abs(((tab[w][1][1][0] - tab[w][1][3][0])**2 + (tab[w][1][1][1] - tab[w][1][3][1])**2)**0.5 - average1) < accuracy):
                pom0 = tab[w][0]
                pom1 = tab[w][1]
                pom2 = []
                pom2.append(pom0)
                pom2.append(pom1)

                wynik.append(pom2)
    usuniete = len(tab)-len(wynik)
    return (wynik,usuniete)

def deleteWindows(tab,frame):
    final = copy.copy(tab)
    global refPt
    bcg = np.zeros((height,width,3))
    bcg = cv2.putText(bcg,"Aby zaznaczyc zbedne okna, kliknij 'u'",(int(width/4),int(height/2)),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,255,200))
    bcg = cv2.putText(bcg,"Aby pominac/zakonczyc, kliknij 'q'",(int(width/3),int(height/2)+50),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,200,255))

    while(True):
        cv2.imshow("Usuwanie Okien Info",cv2.resize(bcg,(outWinSiz[0],outWinSiz[1])))
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            cv2.destroyAllWindows()
            return tab
        if key == ord('u'): 
            cv2.destroyAllWindows()
            break

    cv2.namedWindow("Usuwanie okien")
    cv2.setMouseCallback("Usuwanie okien", mouseAction)

    while(True):
        klatka = copy.copy(frame)
        for w in range(len(final)):
            klatka = cv2.circle(klatka,(final[w][0][0],final[w][0][1]),1,(255,255,255),3)

            klatka = cv2.circle(klatka,(final[w][1][0][0],final[w][1][0][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][1][0],final[w][1][1][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][2][0],final[w][1][2][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][3][0],final[w][1][3][1]),1,(0,0,255),2)

            klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)

        cv2.imshow("Usuwanie okien",cv2.resize(klatka,(outWinSiz[0],outWinSiz[1])))

        if(len(refPt)) == 2 and len(final)>0:

            idU = -1
            refPt2 = [[refPt[0][0],refPt[0][1]],[refPt[1][0],refPt[1][1]]]
            if(refPt2[0][0] > refPt2[1][0]):
                pom = refPt2[0][0]
                refPt2[0][0] = refPt2[1][0]
                refPt2[1][0] = pom
            if(refPt2[0][1] > refPt2[1][1]):
                pom = refPt2[0][1]
                refPt2[0][1] = refPt2[1][1]
                refPt2[1][1] = pom

            refPt2[0][0] = int((refPt2[0][0]/outWinSiz[0])*width)
            refPt2[1][0] = int((refPt2[1][0]/outWinSiz[0])*width)
            refPt2[0][1] = int((refPt2[0][1]/outWinSiz[1])*height)
            refPt2[1][1] = int((refPt2[1][1]/outWinSiz[1])*height)

            for q in range(len(final)):
                if(final[q][0][0] > refPt2[0][0] and final[q][0][0] < refPt2[1][0]):
                    if(final[q][0][1] > refPt2[0][1] and final[q][0][1] < refPt2[1][1]):
                        idU = q

            if(idU != -1):
                us = final.pop(idU)
                print("Usunięto okno o środku w X= ",us[0][0],", Y= ",us[0][1])
                print(refPt)

            refPt = []

        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            cv2.destroyAllWindows()
            return final

def addMissingWindows(tab,frame):
    final = copy.copy(tab)
    global refPt
    bcg = np.zeros((height,width,3))
    bcg = cv2.putText(bcg,"Aby dodac nowe, kliknij 'u'",(int(width/4),int(height/2)),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,255,200))
    bcg = cv2.putText(bcg,"Aby pominac kliknij 'q'",(int(width/3),int(height/2)+50),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,200,255))

    while(True):
        cv2.imshow("Dodawanie Okien Info",cv2.resize(bcg,(outWinSiz[0],outWinSiz[1])))
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            cv2.destroyAllWindows()
            return tab
        if key == ord('u'): 
            cv2.destroyAllWindows()
            break

    cv2.namedWindow("Dodawanie okien")
    cv2.setMouseCallback("Dodawanie okien", mouseAction)

    while(True):
        klatka = copy.copy(frame)
        for w in range(len(final)):
            klatka = cv2.circle(klatka,(final[w][0][0],final[w][0][1]),1,(255,255,255),3)

            klatka = cv2.circle(klatka,(final[w][1][0][0],final[w][1][0][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][1][0],final[w][1][1][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][2][0],final[w][1][2][1]),1,(0,0,255),2)
            klatka = cv2.circle(klatka,(final[w][1][3][0],final[w][1][3][1]),1,(0,0,255),2)

            klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)
            klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)

        cv2.imshow("Dodawanie okien",cv2.resize(klatka,(outWinSiz[0],outWinSiz[1])))

        if(len(refPt)) == 2 and len(final)>0:

            refPt2 = [[refPt[0][0],refPt[0][1]],[refPt[1][0],refPt[1][1]]]
            if(refPt2[0][0] > refPt2[1][0]):
                pom = refPt2[0][0]
                refPt2[0][0] = refPt2[1][0]
                refPt2[1][0] = pom
            if(refPt2[0][1] > refPt2[1][1]):
                pom = refPt2[0][1]
                refPt2[0][1] = refPt2[1][1]
                refPt2[1][1] = pom

            okno = [[int((refPt[0][0]/outWinSiz[0])*width),int((refPt[0][1]/outWinSiz[1])*height)],
                    [int((refPt[1][0]/outWinSiz[0])*width),int((refPt[0][1]/outWinSiz[1])*height)],
                    [int((refPt[0][0]/outWinSiz[0])*width),int((refPt[1][1]/outWinSiz[1])*height)],
                    [int((refPt[1][0]/outWinSiz[0])*width),int((refPt[1][1]/outWinSiz[1])*height)]]
            ctr = [centre(okno)]
            okno = cornerSearch(okno)[0]
            ctr.append(okno)
            final.append(ctr)
            print("Dodano okno o środku w punkcie X= ",ctr[0][0],", Y= ",ctr[0][1],", oraz narożnikach:")
            print(ctr[1])
                #us = final.pop(idU)
                #print("Usunięto okno o środku w X= ",us[0][0],", Y= ",us[0][1])
                #print(refPt)

            refPt = []

        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            cv2.destroyAllWindows()
            return final

def mouseAction(event,x,y,flags,params):
    global refPt,selection
    if(event == cv2.EVENT_LBUTTONDOWN):
        refPt = [(x,y)]
        selection = True
    elif(event == cv2.EVENT_LBUTTONUP):
        refPt.append((x,y))
        selection = False

def arrayTo2dArray(tab,dConst):#zwraca(error,tablica)
    final = []
    #0-brak błędów
    #1-niewłaściwa liczba okien znalezionych
    if (len(tab)) != oknaX*oknaY: return(1,tab)

    first = [[tab[0][0][0],tab[0][0][1]]]

    for w in range(len(tab)):#Znajdowanie kolumn
        id = -1
        for q in range(len(first)):
            dVar = abs(tab[w][0][0] - first[q][0])
            if(dVar < dConst):
                id = w
        if(id == -1): first.append(tab[w][0])
    print(first)

    for w in range(len(first)):#Dopasowywanie okien do kolumn
        pom = []
        for q in range(len(tab)):
            if(abs(first[w][0] - tab[q][0][0]) < dConst):
                a = [tab[q][0]]
                b = tab[q][1]
                a.append(b)
                pom.append(a)
        final.append(pom)

    #Sortowanie wg. wysokości

    for w in range(len(first)):
        for q in range(len(final[w])-1):
            min = final[w][q][1][1]
            id = q
            for e in range(q,len(final[w])):
                if(min > final[w][e][1][1]):
                    min = final[w][e][1][1]
                    id = e
            swap = final[w][q]
            final[w][q] = final[w][id]
            final[w][id] = swap

    return(0,final)

#Tutaj teoretyczny scenariusz
asd = CalibrationBasic2(1)#Wysłanie żądania do kalibracji
print("Kod błędu: ",asd[0])#Feedback
asd = asd[1]#Tablica z oknami
#Okna mają format
#[x,y](środek)
#[x,y],[x,y],[x,y],[x,y](Obramowanie)

wynik = np.zeros((height,width,3))

naroznikiPerspektywa = []
for w in range(len(asd)):
    wynik = cv2.circle(wynik,(int(asd[w][0][0]),int(asd[w][0][1])),3,(255,255,255),3)
    naroznikiPerspektywa.append([int(asd[w][0][0]),int(asd[w][0][1])])
    wynik = cv2.circle(wynik,(int(asd[w][1][0][0]),int(asd[w][1][0][1])),3,(0,0,255),3)
    wynik = cv2.circle(wynik,(int(asd[w][1][1][0]),int(asd[w][1][1][1])),3,(0,255,0),3)
    wynik = cv2.circle(wynik,(int(asd[w][1][2][0]),int(asd[w][1][2][1])),3,(255,0,0),3)
    wynik = cv2.circle(wynik,(int(asd[w][1][3][0]),int(asd[w][1][3][1])),3,(0,255,255),3)

    wynik = cv2.line(wynik,((int(asd[w][1][0][0]),int(asd[w][1][0][1]))),((int(asd[w][1][1][0]),int(asd[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(asd[w][1][2][0]),int(asd[w][1][2][1]))),((int(asd[w][1][1][0]),int(asd[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(asd[w][1][2][0]),int(asd[w][1][2][1]))),((int(asd[w][1][3][0]),int(asd[w][1][3][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(asd[w][1][0][0]),int(asd[w][1][0][1]))),((int(asd[w][1][3][0]),int(asd[w][1][3][1]))),(192,192,192),1)

photoshow(ph_m_m_R,ph_m_m_G,ph_m_m_B)
cv2.waitKey(0)
cv2.destroyAllWindows()

dConst = (((height/oknaY)**2 + (width/oknaX)**2)**0.5)/(6**0.5)
naroznikiPerspektywa = cornerSearch(naroznikiPerspektywa)###Nie zmieniać!

#Obliczanie przekształcenia perspektywicznego
naroznikiPerspektywa[0][0][0] -= int(dConst)
naroznikiPerspektywa[0][0][1] -= int(dConst)
naroznikiPerspektywa[0][1][0] += int(dConst)
naroznikiPerspektywa[0][1][1] -= int(dConst)
naroznikiPerspektywa[0][2][0] += int(dConst)
naroznikiPerspektywa[0][2][1] += int(dConst)
naroznikiPerspektywa[0][3][0] -= int(dConst)
naroznikiPerspektywa[0][3][1] += int(dConst)

pts1 = np.float32([naroznikiPerspektywa[0][0],naroznikiPerspektywa[0][1],naroznikiPerspektywa[0][2],naroznikiPerspektywa[0][3]])
pts2 = np.float32([[0,0],[width,0],[width,height],[0,height]])

M = cv2.getPerspectiveTransform(pts1,pts2)

przekRed = cv2.warpPerspective(ph_m_m_R,M,(width,height))
przekGrn = cv2.warpPerspective(ph_m_m_G,M,(width,height))
przekBlu = cv2.warpPerspective(ph_m_m_B,M,(width,height))

photoshow(przekRed,przekGrn,przekBlu)
cv2.waitKey(0)
cv2.destroyAllWindows()

ph_m_m_R = cv2.warpPerspective(ph_m_m_R,M,(width,height))
ph_m_m_G = cv2.warpPerspective(ph_m_m_G,M,(width,height))
ph_m_m_B = cv2.warpPerspective(ph_m_m_B,M,(width,height))

dsa = CalibrationBasic2(1)
print("Kod błędu: ",dsa[0])
dsa = dsa[1]

wynik = cv2.warpPerspective(ph_NULL,M,(width,height))
wynikCopy = cv2.warpPerspective(ph_NULL,M,(width,height))

dsa = deleteArtifacts(dsa,dConst/2)
print("Usunięto ", dsa[1]," artefaktów")
dsa = dsa[0]

#naroznikiPerspektywa = []
for w in range(len(dsa)):
    wynik = cv2.circle(wynik,(int(dsa[w][0][0]),int(dsa[w][0][1])),3,(255,255,255),3)
    #naroznikiPerspektywa.append([int(dsa[w][0][0]),int(dsa[w][0][1])])
    wynik = cv2.circle(wynik,(int(dsa[w][1][0][0]),int(dsa[w][1][0][1])),3,(0,0,255),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][1][0]),int(dsa[w][1][1][1])),3,(0,255,0),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][2][0]),int(dsa[w][1][2][1])),3,(255,0,0),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][3][0]),int(dsa[w][1][3][1])),3,(0,255,255),3)

    wynik = cv2.line(wynik,((int(dsa[w][1][0][0]),int(dsa[w][1][0][1]))),((int(dsa[w][1][1][0]),int(dsa[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][2][0]),int(dsa[w][1][2][1]))),((int(dsa[w][1][1][0]),int(dsa[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][2][0]),int(dsa[w][1][2][1]))),((int(dsa[w][1][3][0]),int(dsa[w][1][3][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][0][0]),int(dsa[w][1][0][1]))),((int(dsa[w][1][3][0]),int(dsa[w][1][3][1]))),(192,192,192),1)


print("Usuwanie niepoprawnych okien")

dsa = deleteWindows(dsa,wynikCopy)

print("Dodawanie nieznalezionych okien")

dsa = addMissingWindows(dsa,wynikCopy)
wynik = cv2.warpPerspective(ph_NULL,M,(width,height))

for w in range(len(dsa)):
    wynik = cv2.circle(wynik,(int(dsa[w][0][0]),int(dsa[w][0][1])),3,(255,255,255),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][0][0]),int(dsa[w][1][0][1])),3,(0,0,255),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][1][0]),int(dsa[w][1][1][1])),3,(0,255,0),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][2][0]),int(dsa[w][1][2][1])),3,(255,0,0),3)
    wynik = cv2.circle(wynik,(int(dsa[w][1][3][0]),int(dsa[w][1][3][1])),3,(0,255,255),3)

    wynik = cv2.line(wynik,((int(dsa[w][1][0][0]),int(dsa[w][1][0][1]))),((int(dsa[w][1][1][0]),int(dsa[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][2][0]),int(dsa[w][1][2][1]))),((int(dsa[w][1][1][0]),int(dsa[w][1][1][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][2][0]),int(dsa[w][1][2][1]))),((int(dsa[w][1][3][0]),int(dsa[w][1][3][1]))),(192,192,192),1)
    wynik = cv2.line(wynik,((int(dsa[w][1][0][0]),int(dsa[w][1][0][1]))),((int(dsa[w][1][3][0]),int(dsa[w][1][3][1]))),(192,192,192),1)

cv2.imshow("Calibrated Result",cv2.resize(wynik,(outWinSiz[0],outWinSiz[1])))
cv2.waitKey(0)

dalej = arrayTo2dArray(dsa,dConst)[1]

wynik = cv2.warpPerspective(ph_NULL,M,(width,height))

for w in range(len(dalej)):
    for e in range(len(dalej[w])):
        wynik = cv2.circle(wynik,(dalej[w][e][0][0],dalej[w][e][0][1]),1,(255,255,255),1)
        wynik = cv2.circle(wynik,(dalej[w][e][1][0][0],dalej[w][e][1][0][1]),1,(0,0,255),1)
        wynik = cv2.circle(wynik,(dalej[w][e][1][1][0],dalej[w][e][1][1][1]),1,(0,255,0),1)
        wynik = cv2.circle(wynik,(dalej[w][e][1][2][0],dalej[w][e][1][2][1]),1,(255,0,0),1)
        wynik = cv2.circle(wynik,(dalej[w][e][1][3][0],dalej[w][e][1][3][1]),1,(0,255,255),1)

        wynik = cv2.line(wynik,(dalej[w][e][1][0][0],dalej[w][e][1][0][1]),(dalej[w][e][1][1][0],dalej[w][e][1][1][1]),(255,255,255))
        wynik = cv2.line(wynik,(dalej[w][e][1][2][0],dalej[w][e][1][2][1]),(dalej[w][e][1][1][0],dalej[w][e][1][1][1]),(255,255,255))
        wynik = cv2.line(wynik,(dalej[w][e][1][2][0],dalej[w][e][1][2][1]),(dalej[w][e][1][3][0],dalej[w][e][1][3][1]),(255,255,255))
        wynik = cv2.line(wynik,(dalej[w][e][1][0][0],dalej[w][e][1][0][1]),(dalej[w][e][1][3][0],dalej[w][e][1][3][1]),(255,255,255))

        wynik = cv2.putText(wynik,str(w)+","+str(e),(dalej[w][e][0][0],dalej[w][e][0][1]),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,255))

cv2.imshow("Calibrated Result",cv2.resize(wynik,(outWinSiz[0],outWinSiz[1])))
cv2.waitKey(0)
cv2.destroyAllWindows