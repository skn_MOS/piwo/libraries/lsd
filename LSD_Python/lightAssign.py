from imutils.perspective import four_point_transform
from imutils import contours
from skimage import measure
import numpy as np
import imutils
import cv2
import math
import copy

refPt = []

def extract(frame, colour_low, colour_high, thLow, thHigh):#wyciąga kolor ze zdjęcia
	image = cv2.blur(frame,(2,2))#Filtry pierwszego stopnia
	blurred = cv2.GaussianBlur(image, (5,5), 0)
	blurred = cv2.GaussianBlur(blurred, (5,5), 0)
	thresh = cv2.threshold(blurred, thLow, thHigh, cv2.THRESH_BINARY)[1]
	thresh = cv2.GaussianBlur(thresh, (5,5), 0)
	thresh = cv2.GaussianBlur(thresh, (5,5), 0)
	thresh = cv2.erode(thresh, None, iterations=1)
	thresh = cv2.dilate(thresh, None, iterations=2)

	#Odcinanie kolorów poniżej podanego stopnia
	lower = np.array(colour_low)  #-- Niski zakres kolorów-- 
	upper = np.array(colour_high)  #-- Wysoki zakres --
	mask = cv2.inRange(thresh, lower, upper)
	res = cv2.bitwise_and(thresh, thresh, mask= mask)

	#Filtry drugiego stopnia
	res = cv2.GaussianBlur(res, (5,5), 0)
	res = cv2.threshold(res, 155, 255, cv2.THRESH_BINARY)[1]
	res = cv2.erode(res, None, iterations=1)
	res = cv2.dilate(res, None, iterations=2)

	#Filtry trzeciego stopnia
	gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (5,5), 0)
	gray = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)[1]

	#Szukanie okien(Prostokątów)
	cnts = cv2.findContours(gray.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	docCnt = None

	#Zwraca czarno-białe zdjęcie; tablicę z punktami z obwodów okien
	#np.array(y,x,3)
	#tablica[[[x,y],[x,y],[x,y]],[[x,y],[x,y],[x,y]],[[x,y],[x,y],[x,y]]...]
	return (gray,cnts)


def CalibrationBasic(accuracy, frames, expectedWindows, dConst, thLow, thHigh):#accuracy (ilość odtwarzanych sekwencji RGB), Zwraca kod błędu(0-brak), tablicę okien
	if accuracy <= 0: return -1
	if(len(frames) == 0): return -2
	if(len(frames[0]) == 0): return -3
	if(len(frames[1]) == 0): return -4
	if(len(frames[2]) == 0): return -5

	#Wysokość i szerokość klatki
	height, width, ic = frames[0][0].shape

	#dConst = (((height/oknaY)**2 + (width/oknaX)**2)**0.5)/(6**0.5)#Stała odległościowa, jeżeli okna są
	#Dalej niż ta stała, NIE są uznawane za leżące w tym samym miejscu
	roznicaOkien = 0
	error = 0
	#0 - znalezione okna i ilość okien się zgadza
	#Co innego - różnica okien oczekiwanych i znalezionych

	#windowsListMerged = np.zeros((oknaY,oknaX,5), dtype='uint8')#Nieposegregowane
	windowsListMerged = []
	redList = []
	greenList = []
	blueList = []

	for w in range(accuracy):
		redList.append(extract(frames[0][w],(0,0,125),(5,5,255), thLow, thHigh)[1])
		greenList.append(extract(frames[1][w],(0,128,0),(16,255,16), 150, thHigh)[1])
		blueList.append(extract(frames[2][w],(128,0,0),(255,16,16), thLow, thHigh)[1])

	#Kumuluje punkty obwodu z różnych zdjęć
	redCommonPoints = cummulation(redList, accuracy, width, height, thLow, thHigh)
	greenCommonPoints = cummulation(greenList, accuracy, width, height, 150, thHigh)
	blueCommonPoints = cummulation(blueList, accuracy, width, height, thLow, thHigh)

	redGreenCommon = []
	for r in range(len(redCommonPoints)):
		min = width*height
		id = [-1,-1]
		coorRed = centreNP(redCommonPoints[r])

		for g in range(len(greenCommonPoints)):
			coorGreen = centreNP(greenCommonPoints[g])
			distance = ((coorRed[0] - coorGreen[0])**2 + (coorRed[1] - coorGreen[1])**2)**0.5

			if(distance < min and distance < dConst):
				min = distance
				id = [r,g]

		if not(id[0] == -1 or id[1] == -1):
			redGreenCommon.append(id)

	RGComPoints = []
	for w in range(len(redGreenCommon)):
		RGComPoints.append(np.concatenate([redCommonPoints[redGreenCommon[w][0]] , greenCommonPoints[redGreenCommon[w][1]]], axis = 0))

	redGreenBlueCommon = []
	for rg in range(len(RGComPoints)):
		min = width*height
		id = [-1,-1,-1]
		coorRG = centreNP(RGComPoints[rg])

		for b in range(len(blueCommonPoints)):
			coorBlue = centreNP(blueCommonPoints[b])
			distance = ((coorRG[0] - coorBlue[0])**2 + (coorRG[1] - coorBlue[1])**2)**0.5

			if(distance < min and distance < dConst):
				min = distance
				id = [redGreenCommon[rg][0], redGreenCommon[rg][1] ,b]

		if not(id[0] == -1 or id[1] == -1 or id[2] == -1):
			redGreenBlueCommon.append(id)

	RGBComPoints = []
	for w in range(len(redGreenBlueCommon)):
		RGBComPoints.append(np.concatenate([redCommonPoints[redGreenBlueCommon[w][0]], greenCommonPoints[redGreenBlueCommon[w][1]] , blueCommonPoints[redGreenBlueCommon[w][2]]], axis = 0))

	endError =  len(RGBComPoints) - expectedWindows
	return (endError, RGBComPoints)
	

def cummulation(List, accuracy, width, height, thLow, thHigh):
	dConst = 25
	CommonPoints = []
	for r in range(len(List[0])):
		CommonPoints.append(List[0][r])
		centrePoint = centreNP(List[0][r])

		for k in range(1,accuracy):
			min = width*height
			id = -1
			for l in range(len(List[k])):
				localCentre = centreNP(List[k][l])
				distance = ((centrePoint[0] - localCentre[0])**2 + (centrePoint[1] - localCentre[1])**2)**0.5
				if(distance < min and distance < dConst):
					min = distance
					id = l
			if(id != -1):
				#CommonPoints[r].extend(List[k][id])
				CommonPoints[r] = np.concatenate((CommonPoints[r],List[k][id]))
	return CommonPoints

#CornerSearch szuka narożników okien na zasadzie mierzenia odległości punktów z obwodu okna
#do rogów ramki, szuka najmniejszych dystansów
#Można napisac coś optymalniejszego działającego na innej zasadzie, ale po testach to działa
#najlepiej
def cornerSearchNP(tablica, width, height):#Szuka narożników dla tablic NumPy Array
	if(len(tablica) < 4): return (-1)

	#0   1
	#3   2

	narozniki = []
	tab = []
	for w in range(4): narozniki.append([tablica[0][0][0],tablica[0][0][1]])
	for w in range(len(tablica)): tab.append([tablica[w][0][0],tablica[w][0][1]])
	mapCorner = [[0,0],[width,0],[width,height],[0,height]]
	distance = [((tab[0][0] - mapCorner[0][0])**2 + (tab[0][1] - mapCorner[0][1])**2)**0.5,
				((tab[1][0] - mapCorner[1][0])**2 + (tab[1][1] - mapCorner[1][1])**2)**0.5,
				((tab[2][0] - mapCorner[2][0])**2 + (tab[2][1] - mapCorner[2][1])**2)**0.5,
				((tab[3][0] - mapCorner[3][0])**2 + (tab[3][1] - mapCorner[3][1])**2)**0.5]
	id = [0,0,0,0]

	for w in range(1,len(tablica)):
		if (distance[0] > ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5):
			distance[0] = ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5
			narozniki[0][0] = tab[w][0]
			narozniki[0][1] = tab[w][1]
			id[0] = w
		if (distance[1] > ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5):
			distance[1] = ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5
			narozniki[1][0] = tab[w][0]
			narozniki[1][1] = tab[w][1]
			id[1] = w
		if (distance[2] > ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5):
			distance[2] = ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5
			narozniki[2][0] = tab[w][0]
			narozniki[2][1] = tab[w][1]
			id[2] = w
		if (distance[3] > ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5):
			distance[3] = ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5
			narozniki[3][0] = tab[w][0]
			narozniki[3][1] = tab[w][1]
			id[3] = w

	return(narozniki,id)

def cornerSearch(tablica, width, height):#Robi to samo, tylko dla zwyczajnych tablic
	if(len(tablica) < 4): return (-1)

	#0   1
	#3   2

	narozniki = []
	tab = []
	for w in range(4): narozniki.append([tablica[0][0],tablica[0][1]])
	for w in range(len(tablica)): tab.append([tablica[w][0],tablica[w][1]])
	mapCorner = [[0,0],[width,0],[width,height],[0,height]]
	distance = [((tab[0][0] - mapCorner[0][0])**2 + (tab[0][1] - mapCorner[0][1])**2)**0.5,
				((tab[1][0] - mapCorner[1][0])**2 + (tab[1][1] - mapCorner[1][1])**2)**0.5,
				((tab[2][0] - mapCorner[2][0])**2 + (tab[2][1] - mapCorner[2][1])**2)**0.5,
				((tab[3][0] - mapCorner[3][0])**2 + (tab[3][1] - mapCorner[3][1])**2)**0.5]
	id = [0,0,0,0]

	for w in range(len(tablica)):
		if (distance[0] >= ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5):
			distance[0] = ((tab[w][0] - mapCorner[0][0])**2 + (tab[w][1] - mapCorner[0][1])**2)**0.5
			narozniki[0][0] = tab[w][0]
			narozniki[0][1] = tab[w][1]
			id[0] = w
		if (distance[1] >= ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5):
			distance[1] = ((tab[w][0] - mapCorner[1][0])**2 + (tab[w][1] - mapCorner[1][1])**2)**0.5
			narozniki[1][0] = tab[w][0]
			narozniki[1][1] = tab[w][1]
			id[1] = w
		if (distance[2] >= ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5):
			distance[2] = ((tab[w][0] - mapCorner[2][0])**2 + (tab[w][1] - mapCorner[2][1])**2)**0.5
			narozniki[2][0] = tab[w][0]
			narozniki[2][1] = tab[w][1]
			id[2] = w
		if (distance[3] >= ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5):
			distance[3] = ((tab[w][0] - mapCorner[3][0])**2 + (tab[w][1] - mapCorner[3][1])**2)**0.5
			narozniki[3][0] = tab[w][0]
			narozniki[3][1] = tab[w][1]
			id[3] = w
	return(narozniki,id)

def photoshow(FRed,FGreen,FBlue, width, height, thLow, thHigh):
	#Czerwony
	frame = FRed
	cl = [0,0,125]
	ch = [5,5,255]

	out = extract(frame,cl,ch, thLow, thHigh)
	photo = out[0]
	photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

	for w in range(len(out[1])):
		midX = 0
		midY = 0
		for e in range(len(out[1][w])):
			tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
			midX += tupla[0]
			midY += tupla[1]
			cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
		midX /= len(out[1][w])
		midY /= len(out[1][w])
		cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(16,32,255),2)
		nrz = cornerSearchNP(out[1][w])
		if(nrz != -1):
			cv2.circle(photo, (nrz[0][0][0],nrz[0][0][1]), 3, (0, 0, 255), 3)
			cv2.circle(photo, (nrz[0][1][0],nrz[0][1][1]), 3, (0, 255, 0), 3)
			cv2.circle(photo, (nrz[0][2][0],nrz[0][2][1]), 3, (255, 0, 0), 3)
			cv2.circle(photo, (nrz[0][3][0],nrz[0][3][1]), 3, (0, 255, 255), 3)

			cv2.line(photo,(nrz[0][0][0],nrz[0][0][1]),(nrz[0][1][0],nrz[0][1][1]),(255,255,0),2)
			cv2.line(photo,(nrz[0][1][0],nrz[0][1][1]),(nrz[0][2][0],nrz[0][2][1]),(255,255,0),2)
			cv2.line(photo,(nrz[0][2][0],nrz[0][2][1]),(nrz[0][3][0],nrz[0][3][1]),(255,255,0),2)
			cv2.line(photo,(nrz[0][0][0],nrz[0][0][1]),(nrz[0][3][0],nrz[0][3][1]),(255,255,0),2)

	cv2.imshow('Red', cv2.resize(photo,(width, height)))

	#Zielony
	frame = FGreen
	cl = [0,125,0]
	ch = [5,255,5]

	out = extract(frame,cl,ch, thLow, thHigh)
	photo = out[0]
	photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

	for w in range(len(out[1])):
		midX = 0
		midY = 0
		for e in range(len(out[1][w])):
			tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
			midX += tupla[0]
			midY += tupla[1]
			cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
		midX /= len(out[1][w])
		midY /= len(out[1][w])
		cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(32,255,16),2)

	cv2.imshow('Green', cv2.resize(photo,(width, height)))

	#Niebieski
	frame = FBlue
	cl = [125,0,0]
	ch = [255,50,5]

	out = extract(frame,cl,ch, thLow, thHigh)
	photo = out[0]
	photo = cv2.cvtColor(photo, cv2.COLOR_GRAY2BGR)

	for w in range(len(out[1])):
		midX = 0
		midY = 0
		for e in range(len(out[1][w])):
			tupla = (out[1][w][e][0][0], out[1][w][e][0][1])
			midX += tupla[0]
			midY += tupla[1]
			cv2.circle(photo, tupla, 5, (255, 255, 255), 1)
		midX /= len(out[1][w])
		midY /= len(out[1][w])
		cv2.putText(photo, str(w), (int(midX), int(midY)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,16,32),2)

	cv2.imshow('Blue', cv2.resize(photo,(width, height)))

#Usuwanie punktów uznanych za okna, które są za małe, można rozwinąc, brak 100% dokładności
#Accuracy domyślne = szerokość obrazu / (ilość okien * 2)
def deleteArtifacts(baseTab,accuracy, width, height):

	tab = []
	for w in range(len(baseTab)):
		pom0 = centreNP(baseTab[w])
		pom1 = cornerSearchNP(baseTab[w], width, height)[0]
		tab.append([pom0, pom1])

	wynik = []
	average0 = 0
	average1 = 0

	for w in range(len(tab)):
		average0 += ((tab[w][1][0][0] - tab[w][1][2][0])**2 + (tab[w][1][0][1] - tab[w][1][2][1])**2)**0.5
		average1 += ((tab[w][1][1][0] - tab[w][1][3][0])**2 + (tab[w][1][1][1] - tab[w][1][3][1])**2)**0.5
	average0 /= len(tab)
	average1 /= len(tab)
	average2 = ((average0**2 + average1**2)**0.5)*(3**0.5)

	for w in range(len(tab)):
		if(abs(((tab[w][1][0][0] - tab[w][1][2][0])**2 + (tab[w][1][0][1] - tab[w][1][2][1])**2)**0.5 - average0) < accuracy):
			if(abs(((tab[w][1][1][0] - tab[w][1][3][0])**2 + (tab[w][1][1][1] - tab[w][1][3][1])**2)**0.5 - average1) < accuracy):
				pom0 = tab[w][0]
				pom1 = tab[w][1]
				pom2 = []
				pom2.append(pom0)
				pom2.append(pom1)

				wynik.append(pom2)

	for w in range(0,len(wynik)-1):
		for e in range(w+1,len(wynik)):
			if(w < len(wynik)-1 and e < len(wynik)):
				if((wynik[w][0][0] - wynik[e][0][0])**2 + (wynik[w][0][1] - wynik[e][0][1])**2 < average2):
					del wynik[e]



	usuniete = len(tab)-len(wynik)
	return (wynik,usuniete)

def deleteWindows(tab, frame, width, height):
	final = copy.copy(tab)
	global refPt
	bcg = np.zeros((height,width,3))
	bcg = cv2.putText(bcg,"Aby zaznaczyc zbedne okna, kliknij 'u'",(int(width/4),int(height/2)),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,255,200))
	bcg = cv2.putText(bcg,"Aby pominac/zakonczyc, kliknij 'q'",(int(width/3),int(height/2)+50),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,200,255))

	ih,iw,ic = frame.shape
	outWinSiz = [iw,ih]

	while(True):
		cv2.imshow("Usuwanie Okien Info",cv2.resize(bcg,(width,height)))
		key = cv2.waitKey(1) & 0xFF
		if key == ord('q'):
			cv2.destroyAllWindows()
			return tab
		if key == ord('u'): 
			cv2.destroyAllWindows()
			break

	cv2.namedWindow("Usuwanie okien")
	cv2.setMouseCallback("Usuwanie okien", mouseAction)

	while(True):
		klatka = copy.copy(frame)
		for w in range(len(final)):
			klatka = cv2.circle(klatka,(final[w][0][0],final[w][0][1]),1,(255,255,255),3)

			klatka = cv2.circle(klatka,(final[w][1][0][0],final[w][1][0][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][1][0],final[w][1][1][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][2][0],final[w][1][2][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][3][0],final[w][1][3][1]),1,(0,0,255),2)

			klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)

		cv2.imshow("Usuwanie okien",cv2.resize(klatka,(width,height)))

		if(len(refPt)) == 2 and len(final)>0:

			idU = -1
			refPt2 = [[refPt[0][0],refPt[0][1]],[refPt[1][0],refPt[1][1]]]
			if(refPt2[0][0] > refPt2[1][0]):
				pom = refPt2[0][0]
				refPt2[0][0] = refPt2[1][0]
				refPt2[1][0] = pom
			if(refPt2[0][1] > refPt2[1][1]):
				pom = refPt2[0][1]
				refPt2[0][1] = refPt2[1][1]
				refPt2[1][1] = pom

			refPt2[0][0] = int((refPt2[0][0]/width)*outWinSiz[0])
			refPt2[1][0] = int((refPt2[1][0]/width)*outWinSiz[0])
			refPt2[0][1] = int((refPt2[0][1]/height)*outWinSiz[1])
			refPt2[1][1] = int((refPt2[1][1]/height)*outWinSiz[1])

			for q in range(len(final)):
				if(final[q][0][0] > refPt2[0][0] and final[q][0][0] < refPt2[1][0]):
					if(final[q][0][1] > refPt2[0][1] and final[q][0][1] < refPt2[1][1]):
						idU = q

			if(idU != -1):
				us = final.pop(idU)
				print("Usunięto okno o środku w X= ",us[0][0],", Y= ",us[0][1])
				print(refPt)

			refPt = []

		key = cv2.waitKey(1) & 0xFF
		if key == ord('q'):
			cv2.destroyAllWindows()
			return final

def addMissingWindows(tab, frame, width, height):
	final = copy.copy(tab)
	global refPt
	bcg = np.zeros((height,width,3))
	bcg = cv2.putText(bcg,"Aby dodac nowe, kliknij 'u'",(int(width/4),int(height/2)),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,255,200))
	bcg = cv2.putText(bcg,"Aby zakonczyc kliknij 'q'",(int(width/3),int(height/2)+50),cv2.FONT_HERSHEY_SIMPLEX ,1,(200,200,255))

	ih,iw,ic = frame.shape
	outWinSiz = [iw,ih]

	while(True):
		cv2.imshow("Dodawanie Okien Info",cv2.resize(bcg,(width, height)))
		key = cv2.waitKey(1) & 0xFF
		if key == ord('q'):
			cv2.destroyAllWindows()
			return tab
		if key == ord('u'): 
			cv2.destroyAllWindows()
			break

	cv2.namedWindow("Dodawanie okien")
	cv2.setMouseCallback("Dodawanie okien", mouseAction)

	while(True):
		klatka = copy.copy(frame)
		for w in range(len(final)):
			klatka = cv2.circle(klatka,(final[w][0][0],final[w][0][1]),1,(255,255,255),3)

			klatka = cv2.circle(klatka,(final[w][1][0][0],final[w][1][0][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][1][0],final[w][1][1][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][2][0],final[w][1][2][1]),1,(0,0,255),2)
			klatka = cv2.circle(klatka,(final[w][1][3][0],final[w][1][3][1]),1,(0,0,255),2)

			klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][1][0],final[w][1][1][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][2][0],final[w][1][2][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)
			klatka = cv2.line(klatka,(final[w][1][0][0],final[w][1][0][1]),(final[w][1][3][0],final[w][1][3][1]),(255,255,255),1)

		cv2.imshow("Dodawanie okien",cv2.resize(klatka,(width, height)))

		if(len(refPt)) == 2:

			refPt2 = [[refPt[0][0],refPt[0][1]] , [refPt[1][0],refPt[0][1]] , [refPt[1][0],refPt[1][1]] , [refPt[0][0],refPt[1][1]]]

			okno = [[int(((refPt2[0][0]/width)*outWinSiz[0])),int(((refPt2[0][1])/height)*outWinSiz[1])],
					[int(((refPt2[1][0]/width)*outWinSiz[0])),int(((refPt2[1][1])/height)*outWinSiz[1])],
					[int(((refPt2[2][0]/width)*outWinSiz[0])),int(((refPt2[2][1])/height)*outWinSiz[1])],
					[int(((refPt2[3][0]/width)*outWinSiz[0])),int(((refPt2[3][1])/height)*outWinSiz[1])]]

			#refPt2[0][0] = int((refPt2[0][0]/width)*outWinSiz[0])
			#refPt2[1][0] = int((refPt2[1][0]/width)*outWinSiz[0])
			#refPt2[0][1] = int((refPt2[0][1]/height)*outWinSiz[1])
			#refPt2[1][1] = int((refPt2[1][1]/height)*outWinSiz[1])

			okno = cornerSearch(okno, width, height)[0]
			ctr = [centre(okno)]
			ctr.append(okno)
			final.append(ctr)
			print("Dodano okno o środku w punkcie X= ",ctr[0][0],", Y= ",ctr[0][1],", oraz narożnikach:")
			print(ctr[1])

			refPt = []

		key = cv2.waitKey(1) & 0xFF
		if key == ord('q'):
			cv2.destroyAllWindows()
			break
	return final

def mouseAction(event,x,y,flags,params):
	global refPt,selection
	if(event == cv2.EVENT_LBUTTONDOWN):
		refPt = [(x,y)]
		selection = True
	elif(event == cv2.EVENT_LBUTTONUP):
		refPt.append((x,y))
		selection = False

def arrayTo2DArray(windowArray, oknaX, oknaY):
	#Zamienia wykryte okna na tablicę 2D
	#[[centre], [[cor1],[cor2],[cor3],[cor4]]], centre = [x,y]
	tab = copy.copy(windowArray)

	tab.sort(key=lambda x: x[0][1]) #posortowane wg. y
	twoDim = []

	for y in range(oknaY):
		pom = []
		for x in range(oknaX):
			pom.append(tab[y*oknaX + x])
		pom.sort(key=lambda x: x[0][0]) #Posegregowane wg. x
		twoDim.append(pom)

	return twoDim

def centreNP(tablica):
	x = 0
	y = 0
	for w in range(len(tablica)):
		x += tablica[w][0][0]
		y += tablica[w][0][1]
	x = int(x/len(tablica))
	y = int(y/len(tablica))
	return ([x,y])

def centre(tablica):
	x = 0
	y = 0
	for w in range(len(tablica)):
		x += tablica[w][0]
		y += tablica[w][1]
	x = int(x/len(tablica))
	y = int(y/len(tablica))
	return ([x,y])