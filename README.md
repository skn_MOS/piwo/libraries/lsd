# lsd
## Light Show Debug
Library is designed to help debug data from `P.I.W.O.` project.

First designed in Python, then rewritten to C++.
Library is based on `OpenCV`.

### Install `OpenCV`
Cpp, linux
```bash
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install libopencv-dev
```

### Documentation
Documentation can be automatically generated using `doxygen` (only cpp part).
To generate, enter `LSD_Cpp` folder, open terminal (Linux) and enter command
```bash
doxygen lsddox
```
Files will be generated in folder `doxdoc/html`. To acces preview, open `index.html`.

### Features
LSD contains many functions dedicated to debug captured images. Not only is it designed to debug building, but also it can provide tools to manage video inputs and display.
- Play video from given File
- Play video from given source (webcam)
- Print info about video source
- Find and display image from all video capturing devices connected to the host
- Translate color formats from PIWO to BGR, and reversed

### Main functions
LSD's main purpose was to determine RX module location in the building's windows. To have every module assigned, proper order must be followed.
- Set building's dimensions
- Choose proper video input
- Call `firstStep`
- Call `secondStep`
- Call `thirdStep`

First two steps are providing LSD data to properly transorm video from source and find windows' array. The `thirdStep` returns identified window along with certanity. Nothing is perfect, this is why every step is followed by data output for user to make sure no fatal mistakes are made along the way. The `thirdStep` shall be called multiple times to find every module's possition.

### Future
Right now, for the sake of testing and debug, LSD works as independent app. In the future it will be changed to independed library.


### Building test
In folder `LSD_Cpp` open terminal. Apply folowwing commands
```bash
mkdir build
cd build
cmake ..
make
```
After `make` is done, `LSDTest` executable is created and can be executed via `./LSDTest` command.

### Building library
In folder `LSD_Cpp_library` open terminal. Apply folowwing commands
```bash
mkdir build
cd build
cmake ..
make
sudo make install
```

### TODO
- [x] Detecting windows
- [x] Drawing and displaying windows' possitions
- [x] Possible user inaccuracy correction
- [x] Get detected window color
- [x] LIBPIWO color translation
- [x] Return list of all windows within given color range
- [x] Create library
- [ ] DebugIterator
- [ ] Repair accuracy calculation
- [ ] Create function detecting camera movement
- [ ] Create better Haar Cascades
- [ ] API for correctness testing
