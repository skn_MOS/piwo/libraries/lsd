#include "lsd.h"

lsd::lsd()
{
  transformMatrix.centre.x = -1;
  transformMatrix.centre.y = -1;
  for (int i = 0; i < 4; i++)
  {
    inptQ[i] = cv::Point(-1, -1);
    outpQ[i] = cv::Point(-1, -1);
  }
  isTransform = false;
}
