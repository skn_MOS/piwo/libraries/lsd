//LSD
//Light Show Debug
//Jakub Olszewski 2020-2021
//SKN MOS

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/video.hpp>
#include <ctime>
#include <iostream>
#include <cmath>
#include <vector>
#include "lsd.h"

int main()
{
	std::cout << "Jestem Hello World'em" << std::endl;
	lsd foo;
	foo.testEnable();
	foo.setWindowsX(12);
	foo.setWindowsY(10);
	foo.changeMaxFRAMERATE(60);

	cv::VideoCapture cap0("LSD_Test_01.mp4");
	assert(cap0.isOpened());
	//foo.setColorRanges(cap0);

	//foo.getAllVideoInputs();
	//return 0;

	std::cout << foo.PIWODEC2BGR(255) << std::endl;
	std::cout << foo.PIWODEC2BGR(uint32_t(65280)) << std::endl;
	std::cout << foo.PIWODEC2BGR(uint32_t(16711680)) << std::endl;

	std::cout << foo.BGR2PIWODEC(cv::Scalar(0, 0, 255)) << std::endl;
	std::cout << foo.BGR2PIWODEC(cv::Scalar(0, 255, 0)) << std::endl;
	std::cout << foo.BGR2PIWODEC(cv::Scalar(255, 0, 0)) << std::endl;

	//cv::VideoCapture cap0("LSD_Test_01.mp4");
	cv::VideoCapture cap1("LSD_Test_01.mp4");
	cv::VideoCapture cap2("LSD_Test_okno.mp4");
	std::cout << "0" << std::endl;

	assert(cap0.isOpened());
	assert(cap1.isOpened());
	assert(cap2.isOpened());

	std::cout << "a" << std::endl;

	foo.webCamInfo(cap0);
	std::cout << "b" << std::endl;

	foo.firstStep(cap0);
	std::cout << "c" << std::endl;
	foo.secondStep(cap1);
	std::cout << "d" << std::endl;
	finalWin data = foo.thirdStep(cap2);
	std::cout << "Window coordinate: " << data.coordinate << std::endl;
	std::cout << "Choice accuracy: " << data.accuracy << std::endl;
	std::cout << "e" << std::endl;

	cv::Scalar avr = foo.windowAverageColor(cap2, 11, 2);
	std::cout << "R: " << avr[0] << " G: " << avr[1] << " B: " << avr[2] << std::endl;

	std::vector <coorColor> local = foo.whatAreWindowColors(cap2, 16);

	for (int i = 0; i < local.size(); i++)
	{
		std::cout << local[i].coor.x << " " << local[i].coor.y << " " << local[i].color << std::endl;
	}
	std::cout << std::endl;

	std::cout << foo.whatIsMyColor(1, 1, cap2, 16) << std::endl;

	return 0;
}



